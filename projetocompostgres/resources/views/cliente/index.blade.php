@extends('layouts.app')

@section('content')
    <table class="table table-hover">
        <thead>
            <th>Id</th>
            <th>Name</th>
            <th>Edit</th>
            <th>Delete</th>
        </thead>
        <tbody>
            @foreach($clientes as $c)
                <tr>
                    <th>{{$c->id}}</th>
                    <th>{{$c->nome}}</th>
                    <th> <a href="/clients/editCliente/{{$c->id}}"><i class="far fa-edit"></i></a></th>
                    <th> <a href="/clients/deleteClient/{{$c->id}}"><i class="far fa-trash-alt"></i></a></th>
                </tr>

            @endforeach
        </tbody>
    </table>

    <a href="/clients/register" class="btn btn-primary">Register</a>

@endsection