@extends('layouts.app')

@section('content')
    <h1>Register Client</h1>
    <br>
    <form action="/clients/update/{{$client->id}}" method="post">

        <input type="hidden" name="_token" value="{{csrf_token()}}">

        <div class="form-group">
            <input type="text" class="form-control" name="nome" id="nome" value="{{$client->nome}}">
            @if($errors->has('nome'))
                <br>
                <span class="alert alert-danger">{{$errors->first('nome')}}</span>
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>

    </form>


@endsection