@extends('layouts.app')

@section('content')
    <table class="table table-hover">
        <thead>
            <th>Nome</th>
            <th>Descrição</th>
            <th>Valor</th>
            <th>Edit</th>
            <th>Delete</th>
        </thead>
        <tbody>
            @foreach($produtos as $p)
                <tr>
                    <th>{{$p->nome}}</th>
                    <th>{{$p->descricao}}</th>
                    <th>{{$p->valor}}</th>
                    <th> <a href="/produto/produtoEditar/{{$p->id}}"><i class="far fa-edit"></i></a></th>
                    <th> <a href="/produto/produtoExcluir/{{$p->id}}"><i class="far fa-trash-alt"></i></a></th>
                </tr>
            @endforeach
        </tbody>
    </table>

    <a href="/produto/novo" class="btn btn-primary">Cadastrar</a>

@endsection