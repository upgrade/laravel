@extends('layouts.app')

@section('content')

<h1>Registrar Produto</h1>
    <br>
    <form action="/produto/salvar" method="post">

        <input type="hidden" name="_token" value="{{csrf_token()}}">

        <div class="form-group col-md-12">
            <div class="col-md-12">
                <label for="nome">Nome do produto</label>
            </div>
            <div class="col-md-12">
                <input type="text" class="form-control" name="nome" id="nome">
                @if($errors->has('nome'))
                <br>
                    <span class="alert alert-danger">{{$errors->first('nome')}}</span>
                @endif
            </div>
           
        </div>

        <div class="form-group col-md-12">
            <div class="col-md-12">
                <label for="descricao">Descrição do Produto</label>
            </div>
            <div class="col-md-12">
                <textarea rows="5" class="form-control"  name="descricao" id="descricao"></textarea>
                @if($errors->has('descricao'))
                    <br>
                    <span class="alert alert-danger">{{$errors->first('descricao')}}</span>
                @endif
            </div>
           
        </div>

        <div class="form-group col-md-12">
            <div class="col-md-12">
                <label for="valor">Valor</label>
            </div>
            <div class="col-md-12">
            <input type="text" class="form-control" name="valor" id="valor" />
            @if($errors->has('valor'))
                <br>
                <span class="alert alert-danger">{{$errors->first('valor')}}</span>
            @endif
            </div>
          
        </div>

        <div class="form-group col-md-12">
            <button type="submit" class="btn btn-primary">Salvar</button>
        </div>

    </form>

@endsection;