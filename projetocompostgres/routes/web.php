<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

//clientes
Route::get('/clients', 'ClienteController@index');
Route::get('/clients/register', 'ClienteController@new');
Route::get('/clients/editCliente/{id}', 'ClienteController@edit');
Route::get('/clients/deleteClient/{id}', 'ClienteController@delete');
Route::post('/clients/save', 'ClienteController@save');
Route::post('/clients/update/{id}', 'ClienteController@update');

//Produtos
Route::get('/produtos', 'ProdutoController@index');
Route::get('/produto/novo', 'ProdutoController@novo');
Route::get('/produto/produtoEditar/{id}', 'ProdutoController@editar');
Route::get('/produto/produtoExcluir/{id}', 'ProdutoController@excluir');
Route::post('/produto/salvar', 'ProdutoController@salvar');
Route::post('/produto/atualizar/{id}', 'ProdutoController@atualizar');
