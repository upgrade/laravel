<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produto;
use App\Http\Requests\ProdutoRequest;

class ProdutoController extends Controller
{
    public function index(){
        $produtos = Produto::listaProduto();
        return view('produto.index')->with('produtos', $produtos);
    }

    public function novo(){
        return view('produto.formularioProduto');
    }

    public function salvar(ProdutoRequest $request){
        Produto::salvarProduto($request);
        return redirect()->action('ProdutoController@index');
    }

    public function editar($id){
        $produto = Produto::buscarProduto($id);
        return view('produto.editarFormProduto')->with('produto', $produto);
    }

    public function atualizar(ProdutoRequest $request, $id){
        Produto::atualizarProduto($request, $id);
        return redirect()->action('ProdutoController@index');
    }

    public function excluir($id){
        Produto::excluirProduto($id);
        return redirect()->action('ProdutoController@index');
    }

}
