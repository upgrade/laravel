<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Http\Requests\ClienteRequest;

class ClienteController extends Controller
{
    public function index(){
        $clientes = Cliente::clientes();
        return view('cliente.index')->with('clientes', $clientes);
    }

    public function new(){
        return view('cliente.formularioCliente');
    }

    public function save(ClienteRequest $request){
        Cliente::salvarClientes($request);
        return redirect()->action('ClienteController@index');
    }

    public function edit($id){
        //$client  = Cliente::find($id);
        $client = Cliente::buscarCliente($id);
        return view('cliente.editFormClient')->with('client', $client);
    }

    public function update(ClienteRequest $request, $id){
        //Cliente::find($id)->update($request->all());
        Cliente::atualizarCliente($request, $id);
        return redirect()->action('ClienteController@index');

    }

    public function delete($id){
        // $client = Cliente::find($id);
        // $client->delete();
        Cliente::excluirCliente($id);
        return redirect()->action('ClienteController@index');
    }
}
