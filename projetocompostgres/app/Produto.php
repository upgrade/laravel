<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ProdutoRequest;

class Produto extends Model
{
    protected $table = "produto";

    protected $fillable = array('nome', 'descricao', 'valor');

    public static function listaProduto(){
        $produto = DB::table('tb_produto')->select('*')->get();
        return $produto;
    }

    public static function salvarProduto(ProdutoRequest $request){
        DB::table('tb_produto')->insert(
                [
                    'nome' => $request->nome,
                    'descricao' => $request->descricao,
                    'valor' => $request->valor,
                    'created_at' => now(), 
                    'updated_at' => now()
                ]
            );
    }

    public static function buscarProduto($id){
        $produto = DB::table('tb_produto')->where('id', '=',  $id)->first();
        return $produto;
    }

    public static function atualizarProduto(ProdutoRequest $request, $id){
        DB::table('tb_produto')->where('id', '=', $id)
        ->update(
            [
                'nome' => $request->nome,
                'descricao' => $request->descricao,
                'valor' => $request->valor,
                'updated_at' => now()
            ]
            );
    }

    public static function excluirProduto($id){
        DB::table('tb_produto')
            ->where('id', '=', $id)
            ->delete();
    }

}
