<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ClienteRequest;


class Cliente extends Model
{
    //name table
    protected $table = 'tb_cliente';

    //send params for db join with token in form
    protected $fillable = array('nome');


    public static function clientes(){
        $clientes = DB::table('tb_cliente')->select('*')->get();
        return $clientes;
    }


    public static function salvarClientes(ClienteRequest $request){
        DB::table('tb_cliente')->insert(
                ['nome' => $request->nome, 
                'created_at' => now(), 
                'updated_at' => now()
        ]);
    }

    public static function buscarCliente($id){
        $cliente = DB::table('tb_cliente')->where('id', '=', $id)->first();
        return $cliente;
    }


    public static function atualizarCliente(ClienteRequest $request, $id){
        DB::table('tb_cliente')
                    ->where('id', '=', $id)
                    ->update([
                        'nome' => $request->nome,  
                        'updated_at' => now()
                    ]);           
    }

    public static function excluirCliente($id){
        DB::table('tb_cliente')
                ->where('id', '=', $id)
                ->delete();
    }
    
    
}
